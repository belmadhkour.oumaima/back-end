const db = require("../models");
const User = db.users;
const Role = db.role;
const Op = db.Sequelize.Op;


//key+authentificationjwt
const config = require("../config/auth.config");
var jwt = require("jsonwebtoken");
var bcrypt = require("bcryptjs");


//reinitialisation
const crypto = require('crypto');
const nodemailer = require('nodemailer');
const passwordResetToken = db.reset;

//////////////Créer et sauvegarder un nouveau utilisateur//////////////
exports.signup = (req, res) => {
  // Sauvegarder l'utilisateur dans la bd (api)
  User.create({
    matricule: req.body.matricule,
    name: req.body.name,
    email: req.body.email,
    password: bcrypt.hashSync(req.body.password, 8)
  })
    .then(user => {
      if (req.body.roles) {
        Role.findAll({
          where: {
            name: {
              [Op.or]: req.body.roles
            }
          }
        }).then(roles => {
          user.setRoles([[2]]).then(() => {
            res.send({ message: "l'utilisateur ajouté avec succées!" , user });
          })
          .catch(err => {
            res.status(500).send({ message: err.message });
          });
        });
      } else {
        // role = 1 (par défaut prend le role user)
        user.setRoles([1]).then(() => {
          res.send({ message: "l'utilisateur ajouté avec succées!" , user});
        });
      }
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};


/////////// Récupérez tous les utilisateurs de la base de données. /////////////////
exports.findAll = (req, res) => {
 const matricule = req.query.matricule;
  var condition = matricule ? { matricule: { [Op.iLike]: `%${matricule}%`} } : null;
  User.findAll({include: [{model: Role}]}, { where: condition }).then(users=> {
    //parcourir la liste des produits
    //parcourir la liste des roles de chaque utilisateur
    
    var ListUtilisateurs = []
    users.forEach((userr) => {
        userr.roles.forEach((role) => {
            //res.send({ message: "Liste des utilisateurs!" , users , role});
            ListUtilisateurs= users;
            res.send({ message: "Liste des utilisateurs!" , ListUtilisateurs});
            

        });
    });
})
/*
  User.findAll({ where: condition })
    .then(data => {
      res.send(data);
    })*/
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Une erreur s est produite lors de la récupération des utilisateurs"
      });
    });
};

//**************  Trouver un utilisateur avec son id  *******************//
exports.findOne = (req, res) => {
  const id = req.params.id;

  User.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Erreur lors de la récupération de l utilisateur avec l ID=" + id
      });
    });
};
//**************  Trouver un utilisateur avec son matricule *******************//
/*exports.findOne = (req, res) => {
  const matricule = req.params.matricule;

  User.findByMatricule(matricule)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Erreur lors de la récupération de l utilisateur avec la matricule=" + matricule
      });
    });
};*/
//*****************  Mettre à jour un utilisateur par l'identifiant dans la demande ***********//
exports.update = (req, res) => {
  const id = req.params.id;

  User.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "L utilisateur a été mis à jour avec succès."
        });
      } else {
        res.send({
          message: `Impossible de mettre à jour l utilisateur avec l'ID=${id}. L utilisateur est peut-être introuvable ou demande est vide!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Erreur lors de la mise à jour de l utilisateur avec l ID=" + id
      });
    });
};

//********* Supprimer un utilisateur avec l'ID spécifié dans la demande  *********//
exports.delete = (req, res) => {
  const id = req.params.id;

  User.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "L utilisateur a été supprimé avec succès!"
        });
      } else {
        res.send({
          message: `Impossible de supprimer l'utilisateur avec l ID=${id}.`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Impossible de supprimer l'utilisateur avec id" + id
      });
    });
};



////////////////////////  Authentification (as admin/as user) //////////////

exports.signin = (req, res) => {
  
  User.findOne({
    where: {
      matricule: req.body.matricule
    }
  })
    .then(user => {
      if (!user) {
        return res.status(404).send({ message: "Utilisateur n'existe pas." });
      }

      var passwordIsValid = bcrypt.compareSync(
        req.body.password,
        user.password
      );

      if (!passwordIsValid) {
        return res.status(401).send({
          accessToken: null,
          message: "Mot de passe incorrect!"
        });
      }

      var token = jwt.sign({ id: user.id }, config.secret, {
        expiresIn: 86400 // aprés 24 heures déconnexion automatique
      });
     //ROLE
      var authorities = [];
      user.getRoles().then(roles => {
        for (let i = 0; i < roles.length; i++) {
          authorities.push("ROLE_" + roles[i].name.toUpperCase());
        }
        res.status(200).send({
          id: user.id,
          matricule: user.matricule,
          name: user.name,
          email: user.email,
          roles: authorities ,
          accessToken: token
        });
      });
    })
    .catch(err => {
      res.status(500).send({ message: err.message });
    });
};


//*************  Fonction de reinitialisation mot de passe  **************//

exports.ResetPassword= (req, res) => {

  if (!req.body.email) {
    return res.status(500).json({ message: 'L e-mail est obligatoire! ' });
  }

  const user = User.findOne({
     where: {
      email: req.body.email
     }


  }).then( user => {
    if (!user) {
      return res.status(409).json({ message: 'Email n existe pas' });
    } 

    var resettoken = new passwordResetToken(
      { userId: user.id,
        resettoken: crypto.randomBytes(16).toString('hex') });
    
    resettoken.save(function (err) {
          if (err) { return res.status(500).send({ msg: err.message }); }

    passwordResetToken.find(
            { userId: user.id,
              resettoken: { $ne: resettoken.resettoken } }).remove().exec();
          res.status(200).json({ message: 'Reinitialiser mot de passe avec succés.' });

    var transporter = nodemailer.createTransport({
            service: 'Gmail',
            port: 465,
            auth: {
              user: 'assadinour9@gmail.com',
              pass: 'esmanoursoeur***@@@19971997hn'
            }
          });
    var mailOptions = {
            to: user.email,
            from: 'assadinour9@gmail.com',
            subject: 'Node.js Password Reset',
            text: 'You are receiving this because you (or someone else) have requested the reset of the password for your account.nn' +
              'Please click on the following link, or paste this into your browser to complete the process:nn' +
              'http://localhost:4200/response-reset-password/' + resettoken.resettoken + 'nn' +
              'If you did not request this, please ignore this email and your password will remain unchanged.n'
          }
    transporter.sendMail(mailOptions, (err, info) => {
          })
  }).catch(err => {
    res.status(500).send({ message: err.message });
   });

});
} 



//*********** soummettre un nv pass **************//

exports.Validpassword= (req, res) =>{

  if (!req.body.resettoken) {
    return res.status(500).json({ message: 'Token is required' });
  }

  const user = passwordResetToken.findOne({
    resettoken: req.body.resettoken
  });

  if (!user) {
    return res.status(409).json({ message: 'Invalid URL' });
  }

  User.findOneAndUpdate({ id: user.userId }).then(() => {
    res.status(200).json({ message: 'Token verified successfully.' });
  }).catch((err) => {
    return res.status(500).send({ msg: err.message });
  });
}

exports.newPassword = (req, res) =>{
  passwordResetToken.findOne({ resettoken: req.body.resettoken }, function (err, userToken, next) {
    if (!userToken) {
      return res.status(409).json({ message: 'Token has expired' });
    }

  User.findOne({
      id: userToken.userId
    }, function (err, email, next) {
      if (!email) {
        return res
          .status(409)
          .json({ message: 'User does not exist' });
      }
      return bcrypt.hash(req.body.newPassword, 10, (err, hash) => {
        if (err) {
          return res
            .status(400)
            .json({ message: 'Error hashing password' });
        }
        email.password = hash;
        email.save(function (err) {
          if (err) {
            return res
              .status(400)
              .json({ message: 'Password can not reset.' });
          } else {
            userToken.remove();
            return res
              .status(201)
              .json({ message: 'Password reset successfully' });
          }

        });
      });
    });

  }).catch(err => {
    res.status(500).send({ message: err.message });
  });
}




///non pas utilisé///
/*
exports.create = (req, res) => {
  // Validate request
  if (!req.body.matricule) {
    res.status(400).send({
      message: "Le contenu ne peut pas être vide!" //ywaliw f des alerte
    });
    return;
  }
  if (!req.body.name) {
    res.status(400).send({
      message: "Le contenu ne peut pas être vide!" //ywaliw f des alerte
    });
    return;
  }

  if (!req.body.email) {
    res.status(400).send({
      message: "Le contenu ne peut pas être vide!" //ywaliw f des alerte
    });
    return;
  }
  // Créer un utiisateur
  const user = {
    matricule: req.body.matricule,
    name: req.body.name,
    email: req.body.email,
    published: req.body.published ? req.body.published : false
  };

  // Enregistrer l utilisateur dans la bd
  User.create(user)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Une erreur s est produite lors de la création de l utilisateur." //ywaliw f des alerte
      });
    });
};
*/

// Supprimez tous les utilisateurs de la base de données.
/*
exports.deleteAll = (req, res) => {
  User.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} Les utilisateurs ont été supprimés avec succès!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Une erreur s'est produite lors de la suppression de tous les utilisateurs."
      });
    });
};*/

/*trouver tous les utilisateurs publiés
exports.findAllPublished = (req, res) => {
  User.findAll({ where: { published: true } })
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || " Une erreur s'est produite lors de la récupération de l'utilisateurs."
      });
    });
};
*/