const db = require("../models");
const Test = db.test;

const Op = db.Sequelize.Op;
//const config = require("../config/test.config");
const { test } = require("../models");

//Ajout d'un nv test
exports.ajouttest = (req, res) => {
    Test.create({
      nomtest: req.body.nomtest
    })
    .then(test => {
        res.send({ message: "Test est ajouté avec succès!", test});
    })
    .catch(err => {
              res.status(500).send({ message: err.message });
    });
};

//Afficher tous les tests
exports.findAll = (req, res) => {
  const nomtest = req.query.nomtest;
  var condition = nomtest ? { nomtest: { [Op.iLike]: `%${nomtest}%`} } : null;
  Test.findAll({ where: condition })
  .then(data => {
    res.send(data);})
        .catch(err => {
        res.status(500).send({
          message:
            err.message || "Une erreur s'est produite lors de la récupération des produits"
        });
    });
};

//Trouver un test avec son id 
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Test.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Erreur lors de la récupération de test avec l ID=" + id
        });
      });
};

//Mettre à jour un test par l'id
exports.update = (req, res) => {
    const id = req.params.id;
  
    Test.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Le TEST a été mis à jour avec succès."
          });
        } else {
          res.send({
            message: `Impossible de mettre à jour le TEST avec l'ID=${id}. le produit est peut-être introuvable ou demande est vide!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Erreur lors de la mise à jour de TEST avec l ID=" + id
        });
    });
};

//Supprimer un test avec l'ID spécifié dans la demande
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Test.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Test a été supprimé avec succès!"
          });
        } else {
          res.send({
            message: `Impossible de supprimer le Test avec l ID=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Impossible de supprimer le Test avec id" + id
        });
      });
  };