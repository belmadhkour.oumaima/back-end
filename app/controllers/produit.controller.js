const db = require("../models");
const Produit = db.produit;
const Test = db.test;

const Op = db.Sequelize.Op;
const config = require("../config/test.config");
const { produit, test, Sequelize } = require("../models");
const { tests } = require("../config/test.config");



//Ajouter un produit avec leurs tests
exports.ajoutproduit = (req, res) => {
    Produit.create({
      nomproduit: req.body.nomproduit
    })
      .then(produit => {
        console.log(req.body.tests);
        if (req.body.tests) {
          Test.findAll({
            where: {
              nomtest:{[Sequelize.Op.or]: req.body.tests}
              }
          }).then(tests => {
            console.log(tests)
            produit.setTests(tests).then(() => {
              res.send({ message: "Le produit est ajouté avec succès!" , produit});
            })
            .catch(err => {
              res.status(500).send({ message: err.message });
            });
          });
        }
      })
      .catch(err => {
        res.status(500).send({ message: err.message });
      });
};

//Récupérez tous les produits dans bd
exports.findAll = (req, res) => {
    Produit.findAll({include: [{model: Test}]}).then(produit=> {
            //parcourir la liste des produits
            //parcourir la liste des tests de chaque produit
            var ListProduits = []
            produit.forEach((prod) => {
                prod.tests.forEach((test) => {
                  ListProduits= produit;
                    res.send({ message: "Liste des produits!" ,ListProduits});
                });
            });
        })
        .catch(err => {
        res.status(500).send({
          message:
            err.message || "Une erreur s'est produite lors de la récupération des produits"
        });
    });
    
  
};


//Trouver un produit avec son id 
exports.findOne = (req, res) => {
    const id = req.params.id;
  
    Produit.findByPk(id)
      .then(data => {
        res.send(data);
      })
      .catch(err => {
        res.status(500).send({
          message: "Erreur lors de la récupération de la produit avec l ID=" + id
        });
      });
};

//Mettre à jour un produit par l'id
exports.update = (req, res) => {
    const id = req.params.id;
  
    Produit.update(req.body, {
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Le produit a été mis à jour avec succès."
          });
        } else {
          res.send({
            message: `Impossible de mettre à jour le produit avec l'ID=${id}. le produit est peut-être introuvable ou demande est vide!`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Erreur lors de la mise à jour de produit avec l ID=" + id
        });
    });
};

//Supprimer un produit avec l'ID spécifié dans la demande
exports.delete = (req, res) => {
    const id = req.params.id;
  
    Produit.destroy({
      where: { id: id }
    })
      .then(num => {
        if (num == 1) {
          res.send({
            message: "Produit a été supprimé avec succès!"
          });
        } else {
          res.send({
            message: `Impossible de supprimer le produit avec l ID=${id}.`
          });
        }
      })
      .catch(err => {
        res.status(500).send({
          message: "Impossible de supprimer le produit avec id" + id
        });
      });
  };
  
  