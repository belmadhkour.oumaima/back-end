const authJwt = require("./authJWT");
const verication_ajout= require("./verification_ajout");
const verification_ajout_produit= require("./verif-ajout-produit");
const verification_ajout_test = require ("./verif-ajout-test")

module.exports = {
  authJwt,
  verication_ajout,
  verification_ajout_produit,
  verification_ajout_test
};