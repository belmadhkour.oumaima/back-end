const db = require("../models");
const ROLES = db.ROLES;
const User = db.users;

checkDuplicateMatriculeOrEmail = (req, res, next) => {
    // Matricule
    User.findOne({
      where: {
        matricule: req.body.matricule
      }
    }).then(user => {
      if (user) {
        res.status(400).send({
          message: "Erreur! La matricule existe déja!"
        });
        return;
      }
  
      // Email
      User.findOne({
        where: {
          email: req.body.email
        }
      }).then(user => {
        if (user) {
          res.status(400).send({
            message: "Erreur! Email existe déja!"
          });
          return;
        }
  
        next();
      });
    });
  };
  
  checkRolesExisted = (req, res, next) => {
    if (req.body.roles) {
      for (let i = 0; i < req.body.roles.length; i++) {
        if (!ROLES.includes(req.body.roles[i])) {
          res.status(400).send({
            message: "Erreur! Role n'existe pas = " + req.body.roles[i]
          });
          return;
        }
      }
    }
    
    next();
  };
  
  const verification_ajout = {
    checkDuplicateMatriculeOrEmail: checkDuplicateMatriculeOrEmail,
    checkRolesExisted: checkRolesExisted
  };
  
  module.exports = verification_ajout ;