const db = require ("../models");
const Test = db.test;


checkDuplicateNomtest = (req, res, next) => {
    //Nomproduit
    Test.findOne({
      where: {
        nomtest: req.body.nomtest
      }
    }).then(test => {
      if (test) {
        res.status(400).send({
          message: "Erreur! Le test existe déja!"
        });
        return;
      }
  
        next();
    });
};
  
const verification_ajout_test = {
    checkDuplicateNomtest: checkDuplicateNomtest
};
  
module.exports = verification_ajout_test ;