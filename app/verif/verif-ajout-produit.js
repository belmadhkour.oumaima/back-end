const db = require ("../models");
const { produit, TESTS } = require("../models");
const Test = db.TESTS;
const Produit = db.produit;


checkDuplicateNom = (req, res, next) => {
    //Nomproduit
    Produit.findOne({
      where: {
        nomproduit: req.body.nomproduit
      }
    }).then(produit => {
      if (produit) {
        res.status(400).send({
          message: "Erreur! Le produit existe déja!"
        });
        return;
      }
  
        next();
    });
};
  
checkTestsExisted = (req, res, next) => {
    if (req.body.tests) {
      for (let i = 0; i < req.body.tests.length; i++) {
        if (!TESTS.includes(req.body.tests[i])) {
          res.status(400).send({
            message: "Erreur! Test n'existe pas = " + req.body.tests[i]
          });
          return;
        }
      }
    }
    
    next();
};
  
const verification_ajout_produit = {
    checkDuplicateNom: checkDuplicateNom,
    checkTestsExisted: checkTestsExisted
};
  
module.exports = verification_ajout_produit ;