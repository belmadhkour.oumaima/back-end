const dbConfig = require("../config/db.config.js");

const Sequelize = require("sequelize");
const sequelize = new Sequelize(dbConfig.DB, dbConfig.USER, dbConfig.PASSWORD, {
  host: dbConfig.HOST,
  dialect: dbConfig.dialect,
  operatorsAliases: 0,

  pool: {
    max: dbConfig.pool.max,
    min: dbConfig.pool.min,
    acquire: dbConfig.pool.acquire,
    idle: dbConfig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.users = require("./user.model")(sequelize, Sequelize);
db.role = require("./role.model")(sequelize, Sequelize);
db.reset = require("./reset.model")(sequelize, Sequelize);
db.produit = require("./produit.model")(sequelize, Sequelize);
db.test = require("./test.model")(sequelize, Sequelize);



db.produit.belongsToMany(db.test, {
  through: "test_produit",
  foreignKey: "produitId",
  otherKey: "testId"
});

db.test.belongsToMany(db.produit, {
  through: "test_produit",
  foreignKey: "testId",
  otherKey: "produitId"
});

db.reset.belongsTo(db.users, {
  foreignKey: "userId"
})

db.role.belongsToMany(db.users, {
  through: "user_roles",
  foreignKey: "roleId",
  otherKey: "userId"
});

db.users.belongsToMany(db.role, {
  through: "user_roles",
  foreignKey: "userId",
  otherKey: "roleId"
});

db.ROLES = ["user", "admin"];
db.TESTS =  [ "dfa", "usb2" , "usb3", "usb1", "eth"]


module.exports = db;
