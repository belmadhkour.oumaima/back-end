module.exports = (sequelize, Sequelize) => {
    const Produit = sequelize.define("produit", {
      nomproduit: {
        type: Sequelize.STRING
      }
    });  
    return Produit;
  };