const verification_ajout_produit = require ("../verif/verif-ajout-produit")

module.exports = app => {
    const produits = require("../controllers/produit.controller");
    const tests = require("../controllers/produit.controller");
    var router = require("express").Router();
    app.use("/api/produits", router);

 // Création d'un nouveau produits
 router.post("/ajout", [
     verification_ajout_produit.checkDuplicateNom,
     verification_ajout_produit.checkTestsExisted
 ],
 produits.ajoutproduit);

 // Afficher la liste des produits
 router.get("/listeproduits", produits.findAll);


 // Afficher un produit avec son id 
 router.get("/:id",  produits.findOne);

 // Modifier un produit avec son id
 router.put("/:id", [verification_ajout_produit.checkDuplicateNom], produits.update);
   
 // Supprimer un produit avec son id
 router.delete("/:id", produits.delete);

}