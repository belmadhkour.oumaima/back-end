const verification_ajout_test = require ("../verif/verif-ajout-test")
module.exports = app => {
    
    const tests = require ("../controllers/test.controller")
    var router = require("express").Router();
    app.use("/api/tests", router);

 // Création d'un nouveau test
 router.post("/ajouttest", [
     verification_ajout_test.checkDuplicateNomtest
 ],
 tests.ajouttest);

 // Afficher la liste des tests
 router.get("/listetests", tests.findAll);

 // Afficher un test avec son id 
 router.get("/:id", tests.findOne);

 // Modifier un testt avec son id
 router.put("/:id", tests.update);
   
 // Supprimer un test avec son id
 router.delete("/:id", tests.delete);

}