const authJwt = require ("../verif/authJWT")
const roles = require("../controllers/auth.controller");

module.exports = function(app) {

    app.use(function(req, res, next) {
      res.header(
        "Access-Control-Allow-Headers",
        "x-access-token, Origin, Content-Type, Accept"
      );
      next();
    });
  
    app.get(
        "/api/test/user",
        [authJwt.verifyToken , authJwt.isUser],
        roles.userBoard
      );

    app.get(
        "/api/test/admin",
        [authJwt.verifyToken, authJwt.isAdmin],
        roles.adminBoard
      );      
};