const { users } = require("../models");
const verification_ajout = require ("../verif/verification_ajout")
const authJWT = require ("../verif/authJWT")

module.exports = app => {
    const users = require("../controllers/user.controller");
   
    //router ("/api/users/houni nzid les routes l f kol option get /post....")
    var router = require("express").Router();
    app.use("/api/users", router);

  
    // Création d'un nouveau utilisateur -/signup/
    router.post("/signup",  [
      verification_ajout.checkDuplicateMatriculeOrEmail,
      verification_ajout.checkRolesExisted
    ],users.signup);

    //Authentification
    router.post("/signin", users.signin) 
    
    //Mot de passe obulié+recupération
    router.post('req-reset-password', users.ResetPassword)
    router.post('/new-password', users.newPassword);
    router.post('/valid-password-token',users.Validpassword);

    /*
    router.get("/auth/forgot_password", [
       userHandlers.render_forgot_password_template] )
    router.post("/auth/forgot_password" , [
      userHandlers.forgot_password] );
    */
  
    // Afficher la liste des utilisateurs
    router.get("/", users.findAll);
  
    // Afficher un utilisateur avec son id 
    router.get("/:id", users.findOne);
  
    // Modifier un utilisateur avec son id
    router.put("/:id", [
      verification_ajout.checkDuplicateMatriculeOrEmail
    ],users.update);
  
    // Supprimer un utilisateur avec son id
    router.delete("/:id", users.delete);
  
    // Supprimer tous les utilisateurs
    /*router.delete("/", users.deleteAll);*/
  
    
  };
  