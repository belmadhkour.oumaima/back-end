const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const app = express();
const cookieParser = require('cookie-parser');

//FRONT
var corsOptions = {
  origin: "http://localhost:4200"
};

app.use(cors(corsOptions));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
//cookies
app.use(cookieParser());

//connection db
const db = require("./app/models");
const Role = db.roles;
const User = db.role;
const Reset = db.reset;
const Test = db.test;

// simple route pour tester le fonctionnement de node
app.get("/", (req, res) => {
  res.json({ message: "Welcome to pfe application." });
});


//port de exécution
const PORT = process.env.PORT || 8083;
app.listen(PORT, () => {
  console.log(`Server is running on port ${PORT}.`);
});

//création d'une table (condition:  n'existe pas)
db.sequelize.sync();
//supp la table si existe ou pas 
/*db.sequelize.sync({force: true}).then(() => {
  //fonction d'initialisation des valeurs
  initial();
});*/


//Les routes de controller
require("./app/routes/user.routes")(app);
require('./app/routes/auth.routes')(app);
require('./app/routes/produit.routes')(app);
require('./app/routes/test.routes')(app);
require('./app/routes/role.routes')(app);
//ajout dans la table test
/*
function initial() {
  Test.create({
    id: 1,
    nomtest: "dfa"
  });
 
  Test.create({
    id: 2,
    nomtest: "usb2"
  });

  Test.create({
    id: 3,
    nomtest: "usb3"
  });

}*/


/*supprimer la table si existe déja
 db.sequelize.sync({ force: true }).then(() => {
 console.log("Drop and re-sync db.");
 });
 
 db.sequelize.sync();
*/


